﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EmailForFlyersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SampleAPIController : ControllerBase
    {
        [HttpGet("[action]")]
        public IActionResult Index()
        {
            string[] result = { "Sample1", "Sample2", "Sample3" };

            return Ok(result);
        }
    }
}
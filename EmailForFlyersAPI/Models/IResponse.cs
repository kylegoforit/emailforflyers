﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailForFlyersAPI.Models
{
    interface IResponse
    {
        ResponseCode Code { get; set; }
        string ErrorMessage { get; set; }
        Exception Exception { get; set; }
        DateTime ResponseAt { get; set; }
    }

    public enum ResponseCode
    {
        Success = 200,
        Error = 400
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailForFlyersAPI.Models.Account
{
    public class SignInRequest : IRequest
    {
        public UserCredential Credential { get; set; }
        public DateTime RequestAt { get; set; }
        public string IPAddress { get; set; }
    }
}

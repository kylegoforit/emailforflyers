﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailForFlyersAPI.Models
{
    interface IRequest
    {
        DateTime RequestAt { get; set; }
        string IPAddress { get; set; }
    }
}

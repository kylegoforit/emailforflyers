import React, { Component } from 'react';
import { Route, withRouter } from 'react-router';
import { Layout } from './components/Layout';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import SignIn from './components/Login/SignIn';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#757ce8',
            main: '#142d53',
            dark: '#002884',
            contrastText: '#fff'
        },
        secondary: {
            light: '#757ce8',
            main: '#ebd900',
            dark: '#002884',
            contrastText: '#fff'
        },
        inherit: {
            main: "#fff",
        },
        error: {
            main: "#dc3545",
            dark: "#c82333"
        }
    },
    typography: {
        useNextVariants: true
    }
});

class App extends Component {
    static displayName = App.name;

    constructor(props) {
        super(props);

        this.state = {
            pathname: this.props.location.pathname,
            search: '',
            hash: ''
        }
    }

    UNSAFE_componentWillMount() {
        this.unlisten = this.props.history.listen((location, action) => {
            this.setState({
                pathname: location.pathname,
                search: location.search,
                hash: location.hash,
                windowHeight: undefined,
                windowWidth: undefined,
                screenSize: undefined
            });
        });
    }

    componentDidMount() {
        this.handleResize();
        window.addEventListener("resize", this.handleResize);
    }

    componentWillUnmount() {
        this.unlisten();
        window.removeEventListener("resize", this.handleResize);

    }

    handleResize = () => {
        this.setState({
            windowHeight: window.innerHeight,
            windowWidth: window.innerWidth
        });

        const { windowWidth } = this.state;
        let width = windowWidth !== window.innerWidth ? window.innerWidth : windowWidth;


        if (width < 600) {
            this.setState({ screenSize: "xs" });
        } else if (width >= 600 && width < 960) {
            this.setState({ screenSize: "sm" });
        } else if (width >= 960 && width < 1280) {
            this.setState({ screenSize: "md" });
        } else if (width >= 1280 && width < 1920) {
            this.setState({ screenSize: "lg" });
        } else {
            this.setState({ screenSize: "xl" });
        }
    };

    render() {
        const { pathname, screenSize } = this.state;

        return (
            <MuiThemeProvider theme={theme}>
                <Layout pathname={pathname}>
                    <Route exact path='/' component={Home} screenSize={screenSize} />
                    <Route path='/counter' component={Counter} screenSize={screenSize} />
                    <Route path='/fetch-data' component={FetchData} screenSize={screenSize} />
                    <Route path='/account/sign-in' component={() => <SignIn screenSize={screenSize} />} />
                </Layout>
            </MuiThemeProvider>
        );
    }
}

export default withRouter(App);
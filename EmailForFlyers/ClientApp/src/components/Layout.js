import React, { Component } from 'react';
import { Container } from 'reactstrap';
import NavMenuForDesktop from './NavMenu/NavMenuForDesktop';

export class Layout extends Component {
    static displayName = Layout.name;
    constructor(props) {
        super(props);

        this.state = {

        };
    }

    render() {
        const { pathname } = this.props;

        let navMenu = (
                <NavMenuForDesktop>
                    {this.props.children}
                </NavMenuForDesktop>
        );
        let isBeforeLogin = pathname.includes('/account/')
                ? (
                <div>
                    {this.props.children}
                </div>
                )
                : (
                <NavMenuForDesktop>
                    {this.props.children}
                </NavMenuForDesktop>
                );
        return (
            <div>
                {isBeforeLogin}
            </div>
        );
  }
}

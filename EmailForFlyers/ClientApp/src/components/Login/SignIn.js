﻿import "./loginStyle.css";
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

const styles = theme => ({
    signInContainer: {
        backgroundColor: "#fff",
        textAlign: 'center'
    },
    formContainer: {
        padding: '0px 10px',
        [theme.breakpoints.up('lg')]: {
            padding: '10px 100px'
        }
    },
    whiteSpaceContainer: {
        height: "100%",
        backgroundColor: "#ebd900"
    },
    button: {
        margin: "10px auto"
    },
    textAlignLeft: {
        textAlign: 'left'
    },
    balanceBlock: {
        width: "100%",
        [theme.breakpoints.up('lg')]: {
            height: 100
        }
    }
});

class SignIn extends React.Component {
    static displayName = SignIn.name;
    constructor(props) {
        super(props);

        this.state = {
            account: {
                userName: '',
                password: '',
                remember: false
            },
            submitted: false
        }
    }

    handleChange = (event) => {
        const { account } = this.state;
        if (event.target.value === 'remember') {
            account[event.target.value] = event.target.checked;
        }
        else {
            account[event.target.name] = event.target.value;
        }
        this.setState({ account });
    }

    handleSubmit = () => {
        //this.setState({ submitted: true }, () => {
        //    fetch('api/Authentication/SignIn', {
        //        method: 'post',
        //        headers: {
        //            'Accept': 'application/json, text/plain, */*',
        //            'Content-Type': 'application/json'
        //        },
        //        body: JSON.stringify(this.state.account)
        //    })
        //        .then(res => res.json())
        //        .then(res => {
        //            this.setState({
        //                submitted: false
        //            });
        //        });
        //});
    }

    render() {
        const { classes } = this.props;
        const { submitted, account } = this.state;

        return (
            <Grid
                container
            >
                <Grid item sm={12} md={5}>
                    <div className={classes.signInContainer}>
                        <Container>
                            <ValidatorForm
                                ref="form"
                                onSubmit={this.handleSubmit}
                                className={classes.formContainer}
                            >
                                <Grid
                                    container
                                    spacing={3}
                                    direction="row"
                                    justify="center"
                                >
                                    <div className={classes.balanceBlock}></div>
                                    <Grid item xs={12}>
                                        <h2>Logo</h2>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Typography variant="h3" gutterBottom>
                                            Sign In
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextValidator
                                            fullWidth
                                            variant="outlined"
                                            label="User Name"
                                            onChange={this.handleChange}
                                            name="userName"
                                            value={account.userName}
                                            validators={['required', 'isEmail']}
                                            errorMessages={['this field is required', 'email is not valid']}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextValidator
                                            fullWidth
                                            type="password"
                                            variant="outlined"
                                            label="Password"
                                            onChange={this.handleChange}
                                            name="password"
                                            value={account.password}
                                            validators={['required']}
                                            errorMessages={['this field is required']}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.textAlignLeft}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={account.remember}
                                                    onChange={this.handleChange}
                                                    value="remember"
                                                    color="primary"
                                                />
                                            }
                                            label="Remember Me"
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <div className={classes.button}>
                                            <Button
                                                fullWidth
                                                color="primary"
                                                variant="contained"
                                                type="submit"
                                                disabled={submitted}
                                            >
                                                {
                                                    (submitted && 'Signin...')
                                                    || (!submitted && 'Sign In')
                                                }
                                            </Button>
                                        </div>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <a href="#">Forgot password?</a>
                                    </Grid>
                                    <Grid item xs={12}>
                                        Need a Mailchimp account? <a href="#">Create an account</a>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <p>c 2001 - 2019 All Rights Reserved.</p>
                                    </Grid>
                                </Grid>
                            </ValidatorForm>
                        </Container>
                    </div>
                </Grid>
                <Grid item md={7}>
                    <div className={classes.whiteSpaceContainer}>
                    </div>
                </Grid>
            </Grid>
            );
    }
}

SignIn.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignIn);